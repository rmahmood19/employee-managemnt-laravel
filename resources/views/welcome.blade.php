@extends('layouts.app')

@section('title')

<div class="container">

    <div class="intro-img">
      <img src="{{ asset('theme/img/intro-img.svg') }}" alt="" class="img-fluid">
    </div>

    <div class="intro-info">
      <h2>Manage your <br><span>tasks</span><br> efficiently</h2>
      <div>
        <a href="{{ route('login') }}" class="btn-get-started scrollto">Login to see your tasks</a>
        
      </div>
    </div>

  </div>
    
@endsection

@section('content')
<!-- #services -->
@endsection

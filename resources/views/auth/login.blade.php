@extends('layouts.app')
@section('title')
Login
@endsection

@section('content')
<section id="contact">
        <div class="container-fluid">
          <div class="row wow fadeInUp">
  
            <div class="col-lg-6 offset-lg-3">
  
              <div class="form">
              
                <div id="errormessage"></div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                  <div class="form-row">
                    <div class="form-group col-lg-6">
                      <input type="email" name="email" class="form-control" id="email" placeholder="Your Email" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                      
                      @error('email')
                      <div class="validation">
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                    </div>
                  @enderror
                    </div>
                    <div class="form-group col-lg-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" 
                            placeholder="Password"required autocomplete="current-password">
                      <div class="validation"></div>
                    </div>
                  </div>
                  @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                 

                  <div class="text-center"><button type="submit" title="Send Message">Login</button></div>
                </form>
              </div>
            </div>
  
          </div>
  
        </div>
      </section><!-- #contact -->
  

@endsection

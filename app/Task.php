<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name','description','due_date','user_id','status'];

    protected $dates = [
        'due_date'
    ];
}

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Managify Employee Management</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Manjari&display=swap" rel="stylesheet"> 
  

  <!-- Bootstrap CSS File -->
  <link href="{{asset('theme/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{asset('theme/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('theme/lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('theme/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('theme/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('theme/lib/lightbox/css/lightbox.min.css')}}" rel="stylesheet">
  @yield('css')

  <!-- Main Stylesheet File -->
  <link href="{{asset('theme/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
    Theme Name: NewBiz
    Theme URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="{{ route('welcome') }}" class="scrollto"><img src="theme/img/managify_logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="{{ route('home') }}">Dashboard</a></li>
          
        <li class="drop-down"><a href="">
            @auth
            {{ Auth::user()->name }}
            @else
            Login
            @endauth
        </a>
            <ul>
              <li><a href=""{{ route('logout') }}"
                onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">LOG OUT</a></li>
                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                        </form>  
              <li><a href="#">Drop Down 3</a></li>
            </ul>
          </li>
          
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix">
        <header class="section-header">
                <h3  style="color:white;">
                @yield('title')
                </h3>
        </header>
    
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      Services Section
    ============================-->
   @yield('content')

  

  
 



  
    <!--==========================
      Contact Section
    ============================-->

  </main>

  <!--==========================
    Footer
  ============================-->
  <!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="{{asset('theme/lib/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('theme/lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{asset('theme/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('theme/lib/easing/easing.min.js')}}"></script>
  <script src="{{asset('theme/lib/mobile-nav/mobile-nav.js')}}"></script>
  <script src="{{asset('theme/lib/wow/wow.min.js')}}"></script>
  <script src="{{asset('theme/lib/waypoints/waypoints.min.js')}}"></script>
  <script src="{{asset('theme/lib/counterup/counterup.min.js')}}"></script>
  <script src="{{asset('theme/lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('theme/lib/isotope/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('theme/lib/lightbox/js/lightbox.min.js')}}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{asset('theme/contactform/contactform.js')}}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{asset('theme/js/main.js')}}"></script>
  @yield('scripts')

</body>
</html>

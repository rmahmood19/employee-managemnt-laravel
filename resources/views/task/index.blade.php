@extends('layouts.app')

@section('title')
Tasks
@endsection
@section('content')
<section id="services" class="section-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4 text-center">
                <a href="{{route('tasks.create')}}" class="btn-common btn-edit text-center mb-4">Create a new task</a>
            </div>
          
        </div>
        <div class="row">
          <div class="col-12">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">Due date</th>
                  <th scope="col">task Name</th>
                  <th scope="col">Author</th>
                  <th scope="col">Shares</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
          @foreach ($tasks as $task)
                <tr>
                  <th scope="row">{{ $task->due_date->toFormattedDateString() }}</th>
                  <td>{{ $task->name }}</td>
                  <td>Cristina</td>
                  <td>3.417</td>
                  <td>
                  <button type="button" class="btn btn-common btn-edit text-center"><i class="fa fa-edit fa-lg"></i></button>
                  <button type="button" class="btn btn-common btn-delete"><i class="fa fa-trash fa-lg"></i></button>
                  </td>
                </tr>
          @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
        

          
        </div>

       
      </section><!-- #services -->
@endsection

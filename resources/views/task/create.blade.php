@extends('layouts.app')
@section('title')
{{ isset($task) ? 'Update Task' : 'Create Task' }}
@endsection

@section('content')
<section id="contact">
        <div class="container-fluid">
          <div class="row wow fadeInUp">
  
            <div class="col-md-6 offset-md-3">
  
              <div class="form">
              
                <div id="errormessage">
                 
                </div>
              <form method="POST" action="{{isset($task) ? route('tasks.update',$task->id) : route('tasks.create')}}">
                    @csrf
                    @if(isset($task))
                      @method('put')
                    @endif

                  <div class="form-row">
                    <div class="form-group col-lg-8">
                      <label for="name">Task name</label>
                      <input type="text" name="name" class="form-control" id="name" placeholder="Task name"
                    @if(isset($task))
                      value="{{$task->name}}"
                    @endif
                    />
                    </div>
                    <div class="form-group col-lg-8">
                        <label for="description">Description</label>
                       <textarea name='description' class="form-control">@if(isset($task)){{$task->description}}@endif
                       </textarea>
                    </div>
                    <div class="form-group col-lg-8">
                        <label for="due_date">Due date</label>
                            <input type="text" class="form-control" id='due_date' name="due_date"
                          @if(isset($task))
                            value="{{$task->due_date}}"
                          @endif
                            >
                        </div>
                      



                    </div>
                    <div class="form-group col-lg-8">
                        <label for="user_id">Select a user</label>
                        <select class="form-control" name="user_id">
                          <option id="1" value="1">Reaz</option>
                          <option id="2" value="2">bambon</option>
                        </select>
                      </div>
                      <div class="form-group col-lg-8">
                          <button  type="submit" class="btn-common btn-edit text-center form-control">
                            {{ isset($task) ? 'Update' : 'Create Task' }}
                          </button>
                      </div>
                    
                    
                    
          
                 

                  
                </form>
              </div>
            </div>
  
          </div>
  
        </div>
      </section><!-- #contact -->
  

@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    flatpickr('#due_date', {
      
    });
</script> 
@endsection

